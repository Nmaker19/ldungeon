Game = (require 'game')
game = nil

local gameCanvas = nil
local canvasX = 0
local canvasY = 0

function calculateCanvasPosition()
  canvasX = game.loopData.screenWidth / 2 - game.loopData.windowWidth / 2
  canvasY = game.loopData.screenHeight / 2 - game.loopData.windowHeight / 2
end


function love.load()
  love.graphics.setDefaultFilter('nearest', 'nearest')
  love.window.setMode(
    0,
    0,
    {
      fullscreen = true,
      fullscreentype = 'exclusive',
      resizable = false,
      vsync = true,
    }
  )

  game = Game()

  gameCanvas = love.graphics.newCanvas(game.config.windowWidth, game.config.windowHeight)
  calculateCanvasPosition()

  game.state:addState('menu', require 'states.menustate')
  game.state:addState('quit', require 'states.quitstate')
  game.state:addState('newgame', require 'states.newgamestate')
  game.state:current('menu')

  game.input:registerKeys('up', {'w', 'up'})
  game.input:registerKeys('down', {'s', 'down'})
  game.input:registerKeys('left', {'a', 'left'})
  game.input:registerKeys('right', {'d', 'right'})
  game.input:registerKeys('attack', {'g'})
  game.input:registerKeys('secondary', {'h'})

  game.font:addFont('Crang', 50)
  game.font:addFont('Gamer', 20)
  game.font:addFont('Pixeled', 10)
  game.font:addFont('BirchLeaf', 24)
end

function love.update(dt)
  game.loopData.dt = dt
  game.state:update()
end

function love.draw()
  love.graphics.setCanvas(gameCanvas)

  game.state:draw()
  game.input:reset()
  game.state:endLoop()
  love.graphics.setCanvas()
  love.graphics.draw(
    gameCanvas,
    canvasX,
    canvasY
  )
end

function love.keypressed(key, scanCode)
  game.input:keyPress(scanCode)
end

function love.keyreleased(key, scanCode)
  game.input:keyRelease(scanCode)
end

function love.resize(width, height)
  game.loopData.screenWidth = width
  game.loopData.screenHeight = height
end

function love.lowmemory()
  game.state:lowMemory()
end
