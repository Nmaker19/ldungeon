return function()
  local inputHandler = {
    listenKeys = {},
    isPressed = {},
    isDown = {},
    isReleased = {}
  }

  function inputHandler:registerKeys(name, keys)
    for _, key in ipairs(keys) do
      self.listenKeys[key] = name
    end

    self.isPressed[name] = false
    self.isDown[name] = false
    self.isReleased[name] = false
  end

  function inputHandler:keyPress(key)
    if self.listenKeys[key] == nil then return end

    self.isPressed[self.listenKeys[key]] = true
    self.isDown[self.listenKeys[key]] = true
    self.isReleased[self.listenKeys[key]] = false
  end

  function inputHandler:keyRelease(key)
    if self.listenKeys[key] == nil then return end

    self.isPressed[self.listenKeys[key]] = false
    self.isDown[self.listenKeys[key]] = false
    self.isReleased[self.listenKeys[key]] = true
  end

  function inputHandler:reset()
    self.isPressed = {}
    self.isReleased = {}
  end

  return inputHandler
end
