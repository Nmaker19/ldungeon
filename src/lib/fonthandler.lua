return function()
  local fontHandler = {
    fonts = {}
  }

  function fontHandler:addFont(name, size)
    self.fonts[name] = love.graphics.newFont('res/font/' .. name .. '.ttf', size, 'none')
  end

  function fontHandler:set(name)
    love.graphics.setFont(self.fonts[name])
  end

  return fontHandler
end
