return function()
  local StateSystem = {
    currentStateName = '',
    currentState = nil,
    states = {},
    nextState = nil
  }

  function StateSystem:addState(name, state)
    self.states[name] = state
  end

  function StateSystem:current(name)
    if name == nil then
      return self.currentStateName
    end

    self.currentState = self.states[name]()
    self:load()
  end

  function StateSystem:load()
    self.currentState:onLoad()
  end

  function StateSystem:update()
    self.currentState:onUpdate()
  end

  function StateSystem:draw()
    self.currentState:onDraw()
  end

  function StateSystem:lowMemory()
    self.currentState:onLowMemory()
  end

  function StateSystem:next(state)
    self.nextState = state
  end

  function StateSystem:endLoop()
    if self.nextState ~= nil then
      self:current(self.nextState)
    end
  end

  return StateSystem
end
