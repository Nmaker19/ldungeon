return function(data)
  return {
    onLoad = data.onLoad ~= nil
      and data.onLoad
      or function() end,

    onUpdate = data.onUpdate ~= nil
      and data.onUpdate
      or function() end,

    onDraw = data.onDraw ~= nil
      and data.onDraw
      or function() end,

    onLowMemory = data.onLowMemory ~= nil
     and data.onLowMemory
     or function() end,
  }
end
