return function()
  local imageHandler = {
    images = {}
  }

  function imageHandler:addImage(name, fileName)
    self.images[name] = love.graphics.newImage('res/image/' .. fileName)
    self.images[name]:setFilter('nearest')
  end

  function imageHandler:get(name)
    return self.images[name]
  end

  return imageHandler
end
