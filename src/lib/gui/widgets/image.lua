local Element = require 'lib.gui.element'

return function(config)
  local element = Element()

  element.data = {
    image = game.image:get(config.gameImage),
    scaleX = config.scaleX ~= nil and config.scaleX or 1,
    scaleY = config.scaleY ~= nil and config.scaleY or 1
  }

  function element:update()
  end

  function element:draw(box)
    width, height = self.data.image:getDimensions()
    width = width * self.data.scaleX
    height = height * self.data.scaleY

    love.graphics.draw(
      self.data.image,
      box.x + ((box.width / 2) - (width / 2)),
      box.y + ((box.height / 2) - (height / 2)),
      0,
      self.data.scaleX,
      self.data.scaleY
    )
  end

  return element
end
