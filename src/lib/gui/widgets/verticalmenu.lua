local Element = require 'lib.gui.element'

return function(config)
  local element = Element()

  element.data = {
    options = config.options,
    selected = 1
  }

  function element:update()
    if game.input.isPressed.up then
      self.data.selected = math.max(1, self.data.selected - 1)
    end

    if game.input.isPressed.down then
      self.data.selected = math.min(#self.data.options, self.data.selected + 1)
    end

    if game.input.isReleased.attack then
      self.data.options[self.data.selected]:action()
    end
  end

  function element:draw(box)
    local isSelected = false
    game.font:set('BirchLeaf')


    for i, option in ipairs(self.data.options) do
      isSelected = self.data.selected == i

      love.graphics.printf(
        {
          isSelected and game.config.colors.accent or game.config.colors.primary,
          option.label
        },
        box.x,
        box.y + (40 * i),
        game.loopData.windowWidth,
        'center'
      )
    end
  end

  return element
end
