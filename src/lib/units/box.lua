return function(x, y, width, height)
  return {
    x = x ~= nil and x or 0,
    y = y ~= nil and y or 0,
    width = width ~= nil and width or 0,
    height = height ~= nil and height or 0
  }
end
