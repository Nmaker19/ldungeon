local InputHandler = require 'lib.inputhandler'
local FontHandler = require 'lib.fonthandler'
local StateSystem = require 'lib.statesystem'
local ImageHandler = require 'lib.imagehandler'
local config = require 'config'

function generateGameLoopData()
  return {
    windowWidth = config.windowWidth,
    windowHeight = config.windowHeight,
    screenWidth = love.graphics.getWidth(),
    screenHeight = love.graphics.getHeight(),
    dt = 0,
  }
end

return function()
  local game = {
    loopData = generateGameLoopData(),
    input = InputHandler(),
    font = FontHandler(),
    state = StateSystem(),
    image = ImageHandler(),
    config = config
  }

  function game:resetLoopData()
    self.loopData = generateGameLoopData()
  end

  return game
end
