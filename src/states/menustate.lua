local State = require 'lib.state'
local VerticalMenu = require 'lib.gui.widgets.verticalmenu'
local Image = require 'lib.gui.widgets.image'
local Box = require 'lib.units.box'
local MenuOption = require 'lib.units.menuoption'

return function()
  local state = {}

  function state:onLoad()
    game.image:addImage('logo', 'logo.png')

    self.data = {}
    self.data.image = Image({
      gameImage = 'logo',
      scaleX = 2,
      scaleY = 2,
      size = 120
    })

    self.data.menu = VerticalMenu({
      options = {

        MenuOption("Nueva partida", function()
          game.state:next('newgame')
        end),

        MenuOption("Salir", function()
          game.state:next('quit')
        end)

      }
    })
  end

  function state:onUpdate(dt)
    self.data.menu:update(game.loopData)
  end

  function state:onDraw()
    self.data.image:draw(Box(0, 70, game.loopData.windowWidth, 200))

    self.data.menu:draw(Box(0, 300, game.loopData.windowWidth, game.loopData.windowHeight))
  end

  return State(state)
end
