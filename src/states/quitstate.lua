local State = require 'lib.state'

return function()
  local state = {}

  function state:onLoad()
    love.event.quit()
  end

  return State(state)
end
