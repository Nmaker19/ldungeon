local State = require 'lib.state'

return function()
  local state = {}

  return State(state)
end
