return {
  colors = {
    primary = { 0.44, 0.44, 0.44, 1 },
    secondary = { 0.26, 0.26, 0.26, 1 },
    accent = { 1, 1, 1, 1 }
  },
  windowWidth = 600,
  windowHeight = 600,
}
